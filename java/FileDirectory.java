import com.ioservice.IOService;

import java.io.File;

public class FileDirectory {

    public static void main(String[] args) {

        File sourceFile = new File("src/main/resources/fileForReading.txt");
        File fileToClear = new File("src/main/resources/FileToClear.txt");
        File fileToInsert = new File("src/main/resources/writtingFile.txt");

       File [] directory = new File[]{sourceFile, fileToClear, fileToInsert};

        IOService io = new IOService(sourceFile,fileToClear);

        io.writeAndClear();

    }
}
