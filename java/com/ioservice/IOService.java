package com.ioservice;

import java.io.*;

public class IOService {

    private File[] directory;
    private File file;

    public IOService(File files, File file)  {
        if (directory.length >= 1 && directory.length <= Byte.MAX_VALUE) {
            this.directory = directory;
            this.file = file;
        } else if (directory.length == 0) {
            System.out.println("file is empty");;
        }

    }

    public File[] getFiles() {
        return directory;
    }

    public File renewFile() {
        return file;
    }

    public void writeAndClear() {
        try {
            FileOutputStream fos = new FileOutputStream(renewFile());
            byte[] buffer = "".getBytes();
            fos.write(buffer);

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < getFiles().length; i++) {

            try {
                FileInputStream fis = new FileInputStream(getFiles()[i]);
                int integer = -1;
                String str = "";
                while ((integer = fis.read()) != -1) {
                    str = str + (char) integer;
                }

                FileOutputStream fos = new FileOutputStream(renewFile(), true);
                byte[] buffer = str.getBytes();
                fos.write(buffer, 0, buffer.length);

            } catch (FileNotFoundException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void writeToTheFileBeginning() {
        String helpStr = "";
        try {
            FileInputStream fis = new FileInputStream(renewFile());
            int integer = -1;
            while ((integer = fis.read()) != -1) {
                helpStr = helpStr + (char) integer;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileOutputStream fos = new FileOutputStream(renewFile());
            byte[] buffer = "".getBytes();
            fos.write(buffer);

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < getFiles().length; i++) {
            try {
                FileInputStream fis = new FileInputStream(getFiles()[i]);
                int integer = -1;
                String str = "";
                while ((integer = fis.read()) != -1) {
                    str = str + (char) integer;
                }

                FileOutputStream fos = new FileOutputStream(renewFile(), true);
                byte[] buffer = str.getBytes();
                fos.write(buffer, 0, buffer.length);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            FileOutputStream fos = new FileOutputStream(renewFile(), true);
            byte[] buffer = helpStr.getBytes();
            fos.write(buffer, 0, buffer.length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeToTheFileEnd() {
        for (int i = 0; i < getFiles().length; i++) {
            try {
                FileInputStream fis = new FileInputStream(getFiles()[i]);
                int integer = -1;
                String str = "";
                while ((integer = fis.read()) != -1) {
                    str = str + (char) integer;
                }

                FileOutputStream fos = new FileOutputStream(renewFile(), true);
                byte[] buffer = str.getBytes();
                fos.write(buffer, 0, buffer.length);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}